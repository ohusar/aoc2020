with open("input11.txt") as file:
    data = [list(line.strip()) for line in file]


def neighbours(i, j):
    result = 0
    for ii, jj in [
        (1, 0),
        (0, 1),
        (1, 1),
        (1, -1),
        (-1, -0),
        (0, -1),
        (-1, -1),
        (-1, 1),
    ]:
        iii = i + ii
        jjj = j + jj
        while 0 <= iii < len(data) and 0 <= jjj < len(data[iii]):
            if data[iii][jjj] == "#":
                result += 1
                break
            if data[iii][jjj] == "L":
                break
            iii += ii
            jjj += jj
    return result


prev = None

while True:
    res = ""

    for i in range(len(data)):
        for j in range(len(data[i])):
            cell = data[i][j]

            if cell == "L" and neighbours(i, j) == 0:
                cell = "#"
            elif data[i][j] == "#" and neighbours(i, j) >= 5:
                cell = "L"

            res = res + cell

        res = res + "\n"

    res = res[:-1]
    data = [list(line) for line in res.split("\n")]

    if res == prev:
        break

    prev = res

print()
print(res)
print(res.count("#"))
print(sum([data[i].count("#") for i in range(len(data))]))
