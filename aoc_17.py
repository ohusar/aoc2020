data = """#...#.#.
..#.#.##
..#..#..
.....###
...#.#.#
#.#.##..
#####...
.#.#.##.""".split(
    "\n"
)

active_nodes = set()

for i in range(len(data)):
    for j in range(len(data[i])):
        if data[i][j] == "#":
            active_nodes.add((i, j, 0, 0))


def _all_opts():
    opts = set()
    for x in [-1, 0, 1]:
        for y in [-1, 0, 1]:
            for z in [-1, 0, 1]:
                for w in [-1, 0, 1]:
                    if [x, y, z, w] != [0, 0, 0, 0]:
                        opts.add((x, y, z, w))
    return opts


all_options = _all_opts()


def neighbours(x, y, z, w):
    global all_options

    _neigh = set()
    for opt in all_options:
        o1, o2, o3, o4 = opt
        _neigh.add((x + o1, y + o2, z + o3, w+o4))

    return _neigh


for _ in range(6):
    n_dict = {}
    for node in active_nodes:
        for neighbour in neighbours(*node):
            n_dict[neighbour] = n_dict.get(neighbour, 0) + 1

    new_active_nodes = set()
    for node in n_dict:
        if node in active_nodes and n_dict[node] in [2, 3]:
            new_active_nodes.add(node)
        elif node not in active_nodes and n_dict[node] == 3:
            new_active_nodes.add(node)

    active_nodes = new_active_nodes

print(len(active_nodes))
