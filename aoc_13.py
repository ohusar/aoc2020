with open("input13.txt") as file:
    _ = int(file.readline())
    data = [int(x) if "x" not in x else -1 for x in file.readline().split(",")]


num_to_addition = {}
c = 0
for n in data:
    if n != -1:
        num_to_addition[n] = c
    c += 1


def find_closest_for_number(num, timestap):
    m = timestap // num
    if timestap % num == 0:
        return 0
    return (m + 1) * num - timestap


def check(num, timestamp):
    closest = find_closest_for_number(num, timestamp)
    return closest == num_to_addition[num]


# largest_num = max(num_to_addition)
# t = largest_num * 100000000000 - num_to_addition[largest_num]
# while True:
#     if (t + num_to_addition[largest_num]) % (largest_num * 10000000) == 0:
#         print(t)
#
#     results = [check(n, t) for n in num_to_addition]
#     if False not in results:
#         break
#     t += largest_num

print({k: num_to_addition[k] for k in sorted(num_to_addition, key=lambda x: num_to_addition[x])})

a_k = []
n_k = []
y_k = []
z_k = []

for n_i in sorted(num_to_addition):
    n_k.append(n_i)
    a_k.append(num_to_addition[n_i])

N = 1
for v in n_k:
    N *= v

for i in range(len(n_k)):
    y_k.append(N / n_k[i])


result = 0

for i in range(len(n_k)):
    result += a_k[i] * n_k[i] * z_k[i]

print(result)
