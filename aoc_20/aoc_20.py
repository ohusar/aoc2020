import math

data = {}

with open("input20.txt") as file:
    current = []
    current_id = None
    for line in file:
        line = line.strip()

        if not line:
            continue

        if "Tile" in line:
            _, _id = line.split(" ")

            if current_id:
                data[current_id] = current

            current_id = _id[:-1]
            current = []
            continue

        current.append(list(line))

    data[current_id] = current

keys = list(data.keys())
L = len(keys)
N = round(math.sqrt(len(keys)))


def neighbours(i, j):
    global grid

    result = set()
    for ii, jj in [(1, 0), (0, 1), (-1, 0), (0, -1)]:
        iii = i + ii
        jjj = j + jj
        if 0 <= iii < len(grid) and 0 <= jjj < len(grid[iii]):
            result.add(grid[iii][jjj])
    return result


def top_edge(arr):
    return "".join(arr[0])


def bot_edge(arr):
    return "".join(arr[-1])


def right_edge(arr):
    return "".join([row[-1] for row in arr])


def left_edge(arr):
    return "".join([row[0] for row in arr])


def edges(arr):
    return {
        top_edge(arr),
        right_edge(arr),
        bot_edge(arr),
        left_edge(arr),
        top_edge(arr)[::-1],
        right_edge(arr)[::-1],
        bot_edge(arr)[::-1],
        left_edge(arr)[::-1],
    }


def edges_list(arr, rot=0):
    if isinstance(arr, str):
        arr = data[arr]

    top_e = top_edge(arr)
    right_e = right_edge(arr)
    bot_e = bot_edge(arr)
    left_e = left_edge(arr)

    if rot == 0:
        return [top_e, right_e, bot_e, left_e]
    if rot == 1:
        return [left_e[::-1], top_e, right_e[::-1], bot_e]
    if rot == 2:
        return [bot_e[::-1], left_e[::-1], top_e[::-1], right_e[::-1]]
    if rot == 3:
        return [right_e, bot_e[::-1], left_e, top_e[::-1]]
    if rot == 4:
        return [top_e[::-1], left_e, bot_e[::-1], right_e]
    if rot == 5:
        return [right_e[::-1], top_e[::-1], left_e[::-1], bot_e[::-1]]
    if rot == 6:
        return [bot_e, right_e[::-1], top_e, left_e[::-1]]
    if rot == 7:
        return [left_e, bot_e, right_e, top_e]


possible_neighbours = {}
neighbouring_edges = {}

for k1 in data:
    tile1 = data[k1]
    k1_edges = edges(tile1)

    for k2 in data:
        if k1 != k2:
            tile2 = data[k2]
            k2_edges = edges(tile2)

            if k1_edges.intersection(k2_edges):
                curr = possible_neighbours.get(k1, set())
                curr.add(k2)
                possible_neighbours[k1] = curr

                curr_edges = neighbouring_edges.get(k1, set())
                for e in k1_edges.intersection(k2_edges):
                    curr_edges.add(e)
                neighbouring_edges[k1] = curr_edges


def rotate(arr):
    return list(zip(*arr[::-1]))


def flip(arr):
    import numpy

    np_arr = numpy.array(arr)
    np_arr = numpy.flip(np_arr, 1)
    return np_arr.tolist()


def rotation(arr, rot=0):
    if rot > 3:
        arr = flip(arr)
        rot -= 4

    for _ in range(rot):
        arr = rotate(arr)

    return [list(_) for _ in arr]


def prnt(arr, sep=""):
    for line in arr:
        print(sep.join([str(_) if _ is not None else " - " for _ in line]))


def has_none(grid):
    for row in grid:
        for element in row:
            if element is None:
                return True
    return False


def is_right_nei(key1, key2, r1, r2):
    tile1, tile2 = data[key1], data[key2]
    edges1, edges2 = edges_list(tile1, r1), edges_list(tile2, r2)
    if edges1[1] == edges2[3]:
        return True
    return False


def is_bot_nei(key1, key2, r1, r2):
    tile1, tile2 = data[key1], data[key2]
    edges1, edges2 = edges_list(tile1, r1), edges_list(tile2, r2)
    if edges1[2] == edges2[0]:
        return True
    return False


def determine_top_left(refused=[]):
    possible_top_left = list(
        filter(lambda x: len(possible_neighbours[x]) == 2, possible_neighbours)
    )

    candidate = None
    _rotation_right = None
    _rotation_bot = None
    right_nei, bot_nei = None, None

    while possible_top_left:
        candidate = possible_top_left.pop()
        for r1 in [7, 6, 5, 4, 3, 2, 1, 0]:
            n1, n2 = list(possible_neighbours[candidate])

            for r2 in [7, 6, 5, 4 ,3, 2, 1, 0]:
                if is_right_nei(candidate, n1, r1, r2):
                    right_nei = n1
                    _rotation_right = (r1, r2)
                if is_bot_nei(candidate, n1, r1, r2):
                    bot_nei = n1
                    _rotation_bot = (r1, r2)

                if is_right_nei(candidate, n2, r1, r2):
                    right_nei = n2
                    _rotation_right = (r1, r2)
                if is_bot_nei(candidate, n2, r1, r2):
                    bot_nei = n2
                    _rotation_bot = (r1, r2)

            if None not in [right_nei, bot_nei] and right_nei != bot_nei:
                if (candidate, right_nei, bot_nei, _rotation_right, _rotation_bot) not in refused:
                    break

    return candidate, right_nei, bot_nei, _rotation_right, _rotation_bot


done = False
refused = []
while not done:

    grid = [[None] * N for _ in range(N)]
    rotations = [[0] * N for _ in range(N)]

    top_left, right_nei, bot_nei, rot_right, rot_bot = determine_top_left(refused)

    refused.append((top_left, right_nei, bot_nei, rot_right, rot_bot))

    assert rot_right[0] == rot_bot[0]

    grid[0][0] = top_left
    rotations[0][0] = rot_bot[0]

    grid[0][1] = right_nei
    rotations[0][1] = rot_right[1]

    grid[1][0] = bot_nei
    rotations[1][0] = rot_bot[1]

    used = {top_left, right_nei, bot_nei}

    i, j = 0, 2
    found_in_this_cycle = True

    while has_none(grid):
        if not found_in_this_cycle:
            break

        found_in_this_cycle = False

        if grid[i][j] is not None:
            found_in_this_cycle = True

            if j == N - 1:
                j = 0
                i += 1
            else:
                j += 1
            continue

        left_nei = grid[i][j - 1]
        left_nei_rotation = rotations[i][j - 1]
        if left_nei is not None:
            for key2 in possible_neighbours[left_nei]:
                if key2 in used:
                    continue
                for _rotation in [0, 1, 2, 3, 4, 5, 6, 7]:
                    if is_right_nei(left_nei, key2, left_nei_rotation, _rotation):
                        grid[i][j] = key2
                        rotations[i][j] = _rotation
                        used.add(key2)
                        found_in_this_cycle = True
                        break
        else:
            top_nei = grid[i - 1][j]
            top_nei_rotation = rotations[i - 1][j]
            for key2 in possible_neighbours[top_nei]:
                if key2 in used:
                    continue
                for _rotation in [0, 1, 2, 3, 4, 5, 6, 7]:
                    if is_bot_nei(top_nei, key2, top_nei_rotation, _rotation):
                        grid[i][j] = key2
                        rotations[i][j] = _rotation
                        used.add(key2)
                        found_in_this_cycle = True
                        break

    if has_none(grid):
        continue

    prnt(grid, sep=" ")
    print()

    new_data = {}
    for key in data:
        arr = data[key]
        new_arr = [[]]
        for i in range(len(arr)):
            for j in range(len(arr[i])):
                if i > 0 and j > 0 and j != len(arr[i]) - 1 and i != len(arr) - 1:
                    new_arr[-1].append(arr[i][j])

            if 0 < i < len(arr) - 2:
                new_arr.append([])
        new_data[key] = new_arr

    image_size = len(new_data["1951"])
    image = [[None] * (N * image_size) for _ in range(N * image_size)]

    for i in range(len(grid)):
        for j in range(len(grid[i])):
            start_i = i * image_size
            start_j = j * image_size

            tile = rotation(new_data[grid[i][j]], rotations[i][j])

            for ii in range(len(tile)):
                for jj in range(len(tile[ii])):
                    image[start_i + ii][start_j + jj] = tile[ii][jj]


    def is_monster_pattern(i, j):
        global image
        monster_pattern = [
            (0, 18),
            (1, 0),
            (1, 5),
            (1, 6),
            (1, 11),
            (1, 12),
            (1, 17),
            (1, 18),
            (1, 19),
            (2, 1),
            (2, 4),
            (2, 7),
            (2, 10),
            (2, 13),
            (2, 16),
        ]
        try:
            for d_i, d_j in monster_pattern:
                if image[i + d_i][j + d_j] not in ['#', 'O']:
                    return False
            return True
        except IndexError:
            return False


    def mark_monster(i, j):
        global image
        monster_pattern = [
            (0, 18),
            (1, 0),
            (1, 5),
            (1, 6),
            (1, 11),
            (1, 12),
            (1, 17),
            (1, 18),
            (1, 19),
            (2, 1),
            (2, 4),
            (2, 7),
            (2, 10),
            (2, 13),
            (2, 16),
        ]
        for d_i, d_j in monster_pattern:
            image[i + d_i][j + d_j] = 'O'


    had_monster = False
    for rot in [0, 1, 2, 3, 4, 5, 6, 7]:
        image = rotation(image, rot)

        for i in range(len(image)):
            for j in range(len(image)):
                if is_monster_pattern(i, j):
                    had_monster = True
                    mark_monster(i, j)

        if had_monster:
            break

    c = 0
    for i in range(len(image)):
        for j in range(len(image)):
            if image[i][j] == '#':
                c += 1

    if had_monster:
        done = True
    else:
        continue

    print(had_monster)
    print(c)


