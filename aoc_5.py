with open('input5.txt') as file:
    data = file.read().split('\n')


def get_id(string):

    part1 = string[:7]
    part2 = string[7:]

    row = 2**len(part1) - 1
    col = 2**len(part2) - 1

    for i, char in enumerate(part1):
        if char == 'F':
            row -= 2**(len(part1) - 1 - i)
        # elif char == 'B':
        #     low += 2**(len(part1) - 1 - i)

    for i, char in enumerate(part2):
        if char == 'L':
            col -= 2**(len(part2) - 1 - i)

    return row * 8 + col


ids = []

for line in data:
    ids.append(get_id(line))

ids = sorted(ids)
for i in range(len(ids)):
    if ids[i + 1] != ids[i] + 1:
        print( ids[i] + 1)
        break

print(sorted(ids))
