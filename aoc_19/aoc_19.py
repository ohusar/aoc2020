

with open('rules.txt') as rules_file:
    rules = dict()

    # 127: 125 125 | 121 48
    for line in rules_file:
        _id, rest = line.strip().split(':')
        if ' | ' in rest:
            part1, part2 = rest.split(' | ')
            part1 = part1.strip().split(' ')
            part2 = part2.strip().split(' ')

            rules[_id] = [part1, part2]

        elif 'a' in rest:
            rules[_id] = "a"
        elif 'b' in rest:
            rules[_id] = "b"
        else:
            rules[_id] = [rest.strip().split(' ')]
