data = []

graph = {}

with open("input7.txt") as file:
    for line in file:
        n1, nodes_str = line.split(" contain ")
        n1 = n1.replace(" bags", "").replace(" bag", "").strip()

        nodes = []
        nodes_str = (
            nodes_str.replace(" bags", "")
            .replace(" bag", "")
            .replace(".", "")
            .split(", ")
        )
        for node in nodes_str:
            if "no other" in node:
                continue
            num, name, surname = [_.strip() for _ in node.split(" ")]
            nodes.append([int(num), f"{name} {surname}"])

        data.append([n1, nodes])

for row in data:
    name, children = row
    graph[name] = dict()
    for child in children:
        graph[name][child[1]] = child[0]


node_count = 0


def has_shiny_gold_child(n):
    if "shiny gold" in graph[n].keys():
        return 1

    for ch in graph[n]:
        res = has_shiny_gold_child(ch)
        if res:
            return 1

    return 0


def bags_in_node(n):
    res = 0

    for ch in graph[n]:
        res += graph[n][ch]
        c = graph[n][ch] * bags_in_node(ch)
        res += c
    return res


for key in graph:
    node_count += has_shiny_gold_child(key)

print(node_count)
print(bags_in_node("shiny gold"))
