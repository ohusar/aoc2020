rules = {}

valid_nums = set()

# test = "-test"
test = ''

with open(f"rules{test}.txt") as file:
    for line in file:
        name, rest = line.split(":")
        first, second = rest.split(" or ")
        ranges = [
            [int(x) for x in first.split("-")],
            [int(x) for x in second.split("-")],
        ]

        nums = set()

        for n in range(ranges[0][0], ranges[0][1] + 1):
            valid_nums.add(n)
            nums.add(n)

        for n in range(ranges[1][0], ranges[1][1] + 1):
            valid_nums.add(n)
            nums.add(n)

        rules[name] = nums

my_ticket = (
    "163,151,149,67,71,79,109,61,83,137,89,59,53,179,73,157,139,173,131,167"
    if not test
    else "11,12,13"
)

tickets = []

res = 0
with open(f"nearby{test}.txt") as file:
    for line in file:
        valid = True
        for num in [int(x) for x in line.split(",")]:
            if num not in valid_nums:
                valid = False
        if valid:
            tickets.append([int(x) for x in line.split(",")])

tickets.append([int(x) for x in my_ticket.split(",")])

possibilities = dict()

for i in range(len(tickets[0])):
    for key in rules:
        all_tickets_are_valid = True
        for ticket in tickets:
            if ticket[i] not in rules[key]:
                all_tickets_are_valid = False

        if all_tickets_are_valid:
            current = possibilities.get(key, set())
            current.add(i)
            possibilities[key] = current


assigned = set()

for key in sorted(possibilities, key=lambda x: len(possibilities[x])):
    possibilities[key] = possibilities[key] - assigned
    assigned = assigned.union(possibilities[key])

res2 = 1
for key in sorted(possibilities, key=lambda x: len(possibilities[x])):
    print(key, possibilities[key])
    if 'departure' in key:
        res2 *= [int(x) for x in my_ticket.split(",")][possibilities[key].pop()]


print(res2)