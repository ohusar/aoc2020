data = []
possible_changes = []

with open('input8.txt') as file:
    for i, line in enumerate(file):
        ins, num = line.strip().split()
        if ins in ['nop', 'jmp']:
            possible_changes.append(i)
        num = int(num)
        data.append([ins, num])


acc = 0

for index in possible_changes:
    acc = 0
    pointer = 0
    instructions = set()

    if data[index][0] == 'jmp':
        data[index][0] = 'nop'
    elif data[index][0] == 'nop':
        data[index][0] = 'jmp'

    try:
        while pointer not in instructions:
            instructions.add(pointer)
            ins, num = data[pointer]

            if ins == 'nop':
                pointer += 1
            elif ins == 'acc':
                acc += num
                pointer += 1
            elif ins == 'jmp':
                pointer += num
    except IndexError:
        print('IndexError', index)
        if pointer == len(data):
            break
        else:
            continue

    if data[index][0] == 'jmp':
        data[index][0] = 'nop'
    elif data[index][0] == 'nop':
        data[index][0] = 'jmp'

print(acc)

