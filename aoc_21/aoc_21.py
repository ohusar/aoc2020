def _print_dict(d):
    for k in d:
        print(k, '->', d[k])


def _allergen_name_into_key(name):
    return hash(name)


_GID = 0
TEST = ""
# TEST = "-tst"


class Allergen:
    def __init__(self, name):
        self.name = name
        self.foods_present_in: ["Food"] = set()

    def add_food(self, food: "Food"):
        self.foods_present_in.add(food)

    def possible_ingredients(self):
        ingredients_count = dict()
        for food in self.foods_present_in:
            for ingredient in food.ingredients:
                ingredients_count[ingredient] = ingredients_count.get(ingredient, 0) + 1

        foods_count = len(self.foods_present_in)
        ingredients_that_appeared_in_all_foods = filter(
            lambda k: ingredients_count[k] == foods_count, ingredients_count.keys()
        )
        return set(ingredients_that_appeared_in_all_foods)

    def __hash__(self):
        return _allergen_name_into_key(self.name)

    def __repr__(self):
        return self.name


class Food:
    def __init__(self, raw):
        global _GID
        self.id = _GID
        _GID += 1
        self.raw = raw
        self.ingredients = set(self.raw.split())
        self.allergens = set()

    def add_allergen(self, allergen: Allergen):
        self.allergens.add(allergen)

    def has_ingredient(self, ingredient):
        return ingredient in self.ingredients

    def remove_ingredients(self, ingredients):
        self.ingredients -= ingredients

    def __hash__(self):
        return hash(self.raw)

    def __repr__(self):
        return "{} ({})".format(self.id, ', '.join(str(_) for _ in self.allergens))


if __name__ == "__main__":
    foods = dict()
    allergens = dict()
    all_allergens = set()
    all_ingredients = set()

    with open("input21{}.txt".format(TEST)) as file:
        for line in file:
            if "(" in line:
                _food_ingredients, _food_allergens = line.strip().split(" (contains ")
                _food_allergens = _food_allergens[:-1]
                _food_allergens = _food_allergens.replace(",", "")
                _food_allergens = set(_food_allergens.split())
            else:
                _food_ingredients = line.strip()
                _food_allergens = set()

            all_allergens = all_allergens.union(_food_allergens)

            for i in _food_ingredients.split():
                all_ingredients.add(i)

            food = Food(_food_ingredients)
            foods[_food_ingredients] = food

    for _allergen_name in all_allergens:
        allergen = Allergen(_allergen_name)
        allergens[_allergen_name] = allergen

    with open("input21{}.txt".format(TEST)) as file:
        for line in file:
            if "(" in line:
                _food_ingredients, _food_allergens = line.strip().split(" (contains ")
                _food_allergens = _food_allergens[:-1]
                _food_allergens = _food_allergens.replace(",", "")
                _food_allergens = set(_food_allergens.split())
            else:
                _food_ingredients = line.strip()
                _food_allergens = set()

            food = foods[_food_ingredients]
            for a in _food_allergens:
                allergen = allergens[a]
                allergen.add_food(food)
                food.add_allergen(allergen)

    ingredients_that_cant_be_an_allergen = set(all_ingredients)
    for k in allergens:
        p = allergens[k].possible_ingredients()
        ingredients_that_cant_be_an_allergen -= p

    for food in foods.values():
        food.remove_ingredients(ingredients_that_cant_be_an_allergen)

        print(len(food.ingredients) == len(food.allergens))
