required = ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"]


passports = []
valid = 0


def hgt(x):
    if x[-2:] == "cm":

        return 150 <= int(x[:-2]) <= 193
    if x[-2:] == "in":
        return 59 <= int(x[:-2]) <= 76

    return False


def hcl(x):
    allowed = list("1234567890abcdef")

    if x[0] == "#" and len(x) == 7:
        for ch in x[1:]:
            if ch not in allowed:
                return False
        return True

    return False


checks = {
    "byr": lambda x: 1920 <= int(x) <= 2002,
    "iyr": lambda x: 2010 <= int(x) <= 2020,
    "eyr": lambda x: 2020 <= int(x) <= 2030,
    "hgt": hgt,
    "hcl": hcl,
    "ecl": lambda x: x in ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"],
    "pid": lambda x: len(x) == 9 and int(x),
    "cid": lambda x: True
}


def check_valid(passport):
    p_keys = passport.keys()

    for key in required:
        if key not in p_keys:
            return 0

    for key in passport:
        try:
            res = checks[key](passport[key])
            if not res:
                return 0
        except ValueError:
            return 0

    return 1


with open("input4.txt") as file:
    current = dict()
    for line in file:

        if line.strip() == "":
            passports.append(current)
            valid += check_valid(current)
            current = dict()
            continue

        fields = [v.strip() for v in line.strip().split(" ")]
        for field in fields:
            k, v = field.split(":")
            current[k] = v


passports.append(current)
valid += check_valid(current)

print(passports)
print(valid)
