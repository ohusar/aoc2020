import math

with open("input10.txt") as file:
    data = [int(line.strip()) for line in file.readlines()]

data.insert(0, 0)
data.append(max(data) + 3)
data = sorted(data)


ones, twos, threes = 0, 0, 0

parts = []
current = []
for i in range(len(data)):
    current.append(data[i])
    if i == len(data) - 1 or data[i + 1] - data[i] >= 3:
        parts.append(current)
        current = []


def comb(n, k):
    return math.factorial(n) / (math.factorial(k) * math.factorial(n - k))


def count(n):
    minimum = n - 2

    r = 0
    for k in range(minimum, n + 1):
        r += comb(n, k)
    return r


def count_poss_for_part(part):
    if len(part) in [1, 2]:
        return 1

    if len(part) == 3:
        return 2

    return count(len(part) - 2)


result = 1
for p in parts:
    result *= count_poss_for_part(p)


print(result)
