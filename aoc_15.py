num_occurrence = {1: (1, None), 2: (2, None), 16: (3, None), 19: (4, None), 18: (5, None), 0: (6, None)}
num_count = {1: 1, 2: 1, 16: 1, 19: 1, 18: 1, 0: 1}

counter = 7
last_num = 0


while True:
    if None in num_occurrence[last_num]:
        num = 0
    else:
        num = num_occurrence[last_num][0] - num_occurrence[last_num][1]

    num_count[num] = num_count.get(num, 0) + 1
    num_occurrence[num] = (counter, num_occurrence.get(num, (None, ))[0])

    last_num = num

    counter += 1

    if counter == 30000001:
        break


print(last_num)
