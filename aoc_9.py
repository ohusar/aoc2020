from collections import deque

with open("input9.txt") as file:
    data = [int(n.strip()) for n in file.readlines()]


def is_valid(i, pre_len=25):
    preamble = data[i - pre_len : i]
    num = data[i]
    for n1 in range(len(preamble)):
        for n2 in range(len(preamble)):
            if n1 != n2 and preamble[n1] + preamble[n2] == num:
                return True
    return False


index = 25
while is_valid(index):
    index += 1

goal = data[index]


# container = deque()
#
# for element in data:
#     container.append(element)
#

table = dict()
for i in range(len(data)):
    for j in range(i + 1, len(data)):
        table[(i, j)] = sum(data[i: j + 1])

for key in table:
    if table[key] == goal:
        start, stop = key
        print(table[key], key, data[start:stop+1])
        mn, mx = min(data[start:stop+1]), max(data[start:stop+1])
        print(mn + mx)

