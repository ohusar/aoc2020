import itertools

mem = dict()

# If the bitmask bit is 0, the corresponding memory address bit is unchanged.
# If the bitmask bit is 1, the corresponding memory address bit is overwritten with 1.
# If the bitmask bit is X, the corresponding memory address bit is floating.


def apply_mask(m, num):
    num = list(bin(num)[2:].zfill(36))

    floating = []
    for i, value in enumerate(m):
        if value == "1":
            num[i] = 1
        elif value == "X":
            floating.append(i)

    nums = {int("".join([str(_) for _ in num]), 2)}

    l = len(floating)
    options = list(itertools.product([0, 1], repeat=l))

    for option in options:
        for i in range(len(option)):
            num[floating[i]] = option[i]

        nums.add(int("".join([str(_) for _ in num]), 2))

    return nums


if __name__ == "__main__":
    with open("input14.txt") as file:
        mask = None
        for c, line in enumerate(file):
            print(c)
            if "mask" in line:
                _, mask = line.split(" = ")
            else:
                a, n = line.split("] = ")
                n = int(n)
                a = int(a[4:])

                addresses = apply_mask(mask, a)

                for address in addresses:
                    mem[address] = n

    print(sum(mem.values()))
