from collections import deque

with open("input18.txt") as file:
    data = [line.strip().replace(' ', '') for line in file.readlines()]

print(data)


def compute(line):
    stack = deque()

    bracket = 0
    for v in line:
        if v == '(':
            stack.append('(')
            bracket += 1
        elif v == ')':
            bracket -= 1
            b = deque()
            x = stack.pop()
            while x != '(':
                b.appendleft(x)
                x = stack.pop()

            while '+' in b:
                new_b = []
                pos = b.index('+')
                for i in range(len(b)):
                    if i == pos - 1:
                        new_b.append(b[i] + b[i + 2])
                    elif i in [pos, pos + 1]:
                        continue
                    else:
                        new_b.append(b[i])
                b = new_b

            stack.append(eval(''.join([str(x) for x in b])))

        elif v in ['*', '+']:
            stack.append(v)
        else:
            stack.append(int(v))

    while '+' in stack:
        new_stack = []
        pos = stack.index('+')
        for i in range(len(stack)):
            if i == pos - 1:
                new_stack.append(stack[i] + stack[i + 2])
            elif i in [pos, pos+1]:
                continue
            else:
                new_stack.append(stack[i])
        stack = new_stack

    return eval(''.join([str(x) for x in stack]))


# print(compute(data[0]))
print(sum([compute(line) for line in data]))
# print('\n\n'.join([str((line, compute(line))) for line in data]))


